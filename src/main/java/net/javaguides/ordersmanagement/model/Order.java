package net.javaguides.ordersmanagement.model;

import net.javaguides.billmanagement.model.Bill;

import javax.persistence.*;

@Entity
@Table(name = "orders")

//TODO: @Kasia i @Marcin
// - brakuje relacji zwrotnej do Klienta, jakiego typu będzie to relacja ?
// - dodajcie test jednostkowy, który jakieś dane nam będzie generował, użyjcie @Ignore, chcemy sobie to sami uruchamiać kiedy będzie potrzebne

//TODO: @Marcin i @Grzegorz
// - brakuje relacji do pozycji, jakiego typu będzie to relacja?
// - dodajcie test jednostkowy, który jakieś dane nam będzie generował, użyjcie @Ignore, chcemy sobie to sami uruchamiać kiedy będzie potrzebne
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    //TODO: @Tomek i @Marcin
    // - pomyślcie nad lepszą nazwą niż Bill, nie bardzo to pasuje
    // - dodajcie DAO, niech to DAO zwraca nam dla zadanego klienta wszystkie rachunki, albo nawet lepiej sumę wszystkich rachunków, chcemy wiedzeć, czy klient ma dużo kasy do wydania i np zaproponować mu droższe wino!!! :)
    @OneToOne(cascade = CascadeType.ALL)
    private Bill bill;

    public Order() {
    }



    public Order withId(int id) {
        setId(id);
        return this;
    }

    public Order build() {

        return this;
    }


    public int getId() {
        return id;
    }

    public Order setId(int id) {
        this.id = id;
        return this;
    }


}
