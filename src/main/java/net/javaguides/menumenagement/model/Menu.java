package net.javaguides.menumenagement.model;

import net.javaguides.positionmanagement.model.Position;
import net.javaguides.restaurantmanagement.model.Restaurant;

import javax.persistence.*;
import java.util.List;


/**
 * Menu.java
 * This is a model class represents a User entity
 * @author Ramesh Fadatare
 *
 */

//TODO: @Przemek i Sonia
// - dodajcie test jednostkowy, który jakieś dane nam będzie generował, użyjcie @Ignore, chcemy sobie to sami uruchamiać kiedy będzie potrzebne

@Entity
@Table(name="menus")
public class Menu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    protected int id;

    @Column(name = "name")
    protected String name;


    //TODO: @Przemek i @Sonia:
    // - current menu powinno być typu boolean. To pole w klasie ma wskazywać czy to jest aktualnie menu. Będziemy je sobie wybierać z bazy za pomocą mniej więcej takiego zapytania "from Menu where current = true"
    @Column(name= "currentMenu")
    protected List<Position> currentMenu;

    //TODO: @Ania i @Przemek
    // - Menu przynależy tylko de jednej restauracji, czyż nie? Macie tutaj zły typ relacji, powinien być @OneToMany, albo @OneToOne
    @ManyToOne(cascade = CascadeType.ALL)
    private Restaurant restaurant;

    //TODO: @Przemek i @Sonia:
    // - tutaj jest dobry typ relacji, zaimplementujcie w klasie Position, relację zwrotną, użyjcie atrybutu mappedBy przy definiowaniu relacji
    @ManyToMany(cascade = CascadeType.ALL)
    private List<Position> positions;


    //TODO: @Przemek i @Sonia:
    // - dobrze zaimplementowany wzorzec budowniczego
    // - zastanówcie się, czy na przy dodawaniu nowej pozycji do menu, trzeba przekazywać całą listę (łącznie z obecnymi pozycjami. Jakie ryzyko niesie i jak można to inaczej rozwiązać.
    public Menu() {
    }

    public Menu withPositions(List<Position> positions){
        setPositions(positions);
        return this;
    }

    public Menu withName(String name) {
        setName(name);
        return this;
    }

    public Menu withId(int id) {
        setId(id);
        return this;
    }

    public Menu withCurrentMenu(List<Position> currentMenu){
        setCurrentMenu(currentMenu);
        return this;
    }


    public Menu build() {
        //actions on properties
        return this;
    }

    public List<Position> getCurrentMenu() {
        return currentMenu;
    }

    public void setCurrentMenu(List<Position> currentMenu) {
        this.currentMenu = currentMenu;
    }

    public List<Position> getPositions() {
        return positions;
    }

    public void setPositions(List<Position> positions) {
        this.positions = positions;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}