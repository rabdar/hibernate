package net.javaguides.technical.dao;

import net.javaguides.technical.utl.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.function.Consumer;

public class GenericDao<T> {
    /**
     * Save Entity
     * @param entity
     */
    public void save(T entity) {
        operation(session -> session.save(entity));
    }

    /**
     * Update Entity
     * @param entity
     */
    public void update(T entity) {
        operation(session -> session.update(entity));
    }

    /**
     * Delete Entity
     * @param entity
     */
    public void delete(int entity) {
        Class<T> type = getSupportedClass();

        operation(session -> session.delete(session.get(type, entity)));
    }

    /**
     * Get User By ID
     * @param id
     * @return
     */
    public T get(int id) {

        Class<T> type = getSupportedClass();

        Transaction transaction = null;
        T user = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // get an user object
            user = session.get(type, id);
            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return user;
    }

    private Class<T> getSupportedClass() {
        return (Class<T>) getClass().getGenericSuperclass();
    }

    /**
     * Get all entities
     * @return
     */
    @SuppressWarnings("unchecked")
    public List< T > getAll() {

        Class<T> type = getSupportedClass();

        Transaction transaction = null;
        List < T > result = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // get an user object

            result = session.createQuery("from " + type.getName()).getResultList();

            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return result;
    }

    protected void operation(Consumer<Session> operation) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // save the student object

            operation.accept(session);

            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
}
