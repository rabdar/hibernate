package net.javaguides.addressmanagement.dao;

import net.javaguides.addressmanagement.model.Address;
import net.javaguides.technical.dao.GenericDao;
import net.javaguides.technical.utl.HibernateUtil;

/**
 * CRUD database operations
 * @author Ramesh Fadatare
 *
 */
public class AddressDao extends GenericDao<Address> {

}