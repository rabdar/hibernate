package net.javaguides.emailmanagement.dao;

import net.javaguides.technical.dao.GenericDao;
import net.javaguides.emailmanagement.model.EmailAddress;
import net.javaguides.technical.utl.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class EmailAddressDao extends GenericDao<EmailAddress> {
    public List<EmailAddress> getByEmailAddress(String param) {
        Transaction transaction = null;
        List< EmailAddress > result = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // get an user object

            Query query = session.createQuery("from EmailAddress where emailAddress = :param1");
            query.setParameter("param1", param);

            result = query.getResultList();

            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return result;
    }
}
