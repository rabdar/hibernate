package net.javaguides.emailmanagement.model;

import net.javaguides.usermanagement.model.User;

import javax.persistence.*;

@Entity
@Table(name="emails")
public class EmailAddress {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    protected int id;

    @Column(name = "emailAddress")
    private String emailAddress;

    @OneToOne(cascade = CascadeType.ALL)
    User owner;

    public EmailAddress(){

    }

    public EmailAddress withId(int id){
        setId(id);
        return this;
    }

    public EmailAddress withEmailAddress(String emailAddress){
        setEmailAddress(emailAddress);
        return this;
    }

    public EmailAddress withOwner(User owner){
        setOwner(owner);
        return this;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
}
