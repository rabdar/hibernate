package net.javaguides.usermanagement.dao;

import net.javaguides.technical.dao.GenericDao;

import net.javaguides.usermanagement.model.User;

/**
 * CRUD database operations
 * @author Ramesh Fadatare
 *
 */
public class UserDao extends GenericDao<User> {

}