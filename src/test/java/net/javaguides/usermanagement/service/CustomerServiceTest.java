package net.javaguides.usermanagement.service;

import net.javaguides.customermanagement.model.Customer;
import net.javaguides.ordersmanagement.model.Order;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

public class CustomerServiceTest {
    CustomerService customerService = new CustomerService();

    @Test
    void getNoDiscountIfUserHasLessThan5Orders() {
        //given
        Customer customer = Mockito.mock(Customer.class);
        List<Order> mockedOrders = getOrders(0);
        Mockito.when(customer.getOrders()).thenReturn(mockedOrders);

        //when
        int discount = customerService.getDiscount(customer);

        //then
        Assertions.assertEquals(0, discount);
    }

    @Test
    void get5PercentDiscountIfUserHas5To2Orders() {
        //given
        Customer customer = Mockito.mock(Customer.class);
        List<Order> mockedOrders = getOrders(5);

        Mockito.when(customer.getOrders()).thenReturn(mockedOrders);

        //when
        int discount = customerService.getDiscount(customer);

        //then
        Assertions.assertEquals(5, discount);
    }

    @Test
    void get10PercentDiscountIfUserHas20To50Orders() {
        //given
        Customer customer = Mockito.mock(Customer.class);
        List<Order> mockedOrders = getOrders(20);

        Mockito.when(customer.getOrders()).thenReturn(mockedOrders);

        //when
        int discount = customerService.getDiscount(customer);

        //then
        Assertions.assertEquals(10, discount);
    }

    @Test
    void get15PercentDiscountIfUserHasMoreThan50Orders() {
        //given
        Customer customer = Mockito.mock(Customer.class);
        List<Order> mockedOrders = getOrders(50);

        Mockito.when(customer.getOrders()).thenReturn(mockedOrders);

        //when
        int discount = customerService.getDiscount(customer);

        //then
        Assertions.assertEquals(15, discount);
    }

    private List<Order> getOrders(int countOfOrders) {
        List<Order> mockedOrders = new ArrayList<>();
        for(int i=0;i<countOfOrders;i++){
            mockedOrders.add(new Order());
        }
        return mockedOrders;
    }

}
