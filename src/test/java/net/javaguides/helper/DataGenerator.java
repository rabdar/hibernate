package net.javaguides.helper;

import net.javaguides.customermanagement.model.Customer;
import net.javaguides.technical.dao.GenericDao;
import net.javaguides.restaurantmanagement.model.Restaurant;
import org.junit.jupiter.api.Test;

public class DataGenerator {

    @Test
    void generateRestaurant() {
        GenericDao<Restaurant> dao = new GenericDao();

        Restaurant restaurant = new Restaurant();
        restaurant.setRestaurantName("Restauracja Łowicka");

        dao.save(restaurant);
    }
    @Test
    void generateCustomers() {
        GenericDao<Customer> dao = new GenericDao<Customer>();
        GenericDao<Restaurant> restaurantDao = new GenericDao<Restaurant>();

        int count = 6;
        for(int i = 0; i<count; i++){
            Customer customer = new Customer();
            customer.setFirstName(String.format("Name%d", i));
            customer.setLastName(String.format("LastName%d", i));
            customer.setRestaurant(restaurantDao.get(1));
            dao.save(customer);
        }
    }
}
