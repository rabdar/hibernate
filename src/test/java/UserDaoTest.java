import net.javaguides.usermanagement.dao.UserDao;
import net.javaguides.usermanagement.model.User;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

public class UserDaoTest {
    UserDao userDao = new UserDao();

    @Test
    public void userShouldBeSaved(){
        //given
        User user = new User()
                .withEmail("user@gmail.com")
                .withName("User")
                .build();

        //when
        userDao.save(user);

        //then
        List<User> users = userDao.getAll().stream()
                .filter(u -> u.getName().equalsIgnoreCase("User"))
                .collect(Collectors.toList());

        Assert.assertEquals(users.size(),  1);
    }

//    @Test
//    public void displayMenu(){
//        //given
//        //tutaj tworze menu
//        //pozycja ze wszystkimi dostępnymi produktami: pierogi (mąka, ziemniaki)
//        Position pierogi = new Position();
//        //pozycja bez wszystkich składników: naleśniki(mąka, ser)
//        Position naleśniki = new Position();
//
//        //when
//        Menu currentMenu = MenuService.getCurrentMenu();
//
//        //then
//        List<Position> positions = currentMenu.getPositions();
//        Assertions.assertEquals(positions.get(0).getId(), pierogi.getId());
//    }
}
